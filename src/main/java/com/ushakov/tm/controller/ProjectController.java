package com.ushakov.tm.controller;

import com.ushakov.tm.api.IProjectController;
import com.ushakov.tm.api.IProjectService;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String projectDescription = TerminalUtil.nextLine();
        final Project project = projectService.add(projectName, projectDescription);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }

}
