package com.ushakov.tm.controller;

import com.ushakov.tm.api.ITaskController;
import com.ushakov.tm.api.ITaskService;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.service.TaskService;
import com.ushakov.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String taskDescription = TerminalUtil.nextLine();
        final Task task = taskService.add(taskName, taskDescription);
        if (task == null) {
            System.out.println("[FAILED]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK]");
    }

}
