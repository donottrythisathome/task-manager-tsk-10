package com.ushakov.tm.service;

import com.ushakov.tm.api.ICommandRepository;
import com.ushakov.tm.api.ICommandService;
import com.ushakov.tm.model.Command;
import com.ushakov.tm.repository.CommandRepository;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
